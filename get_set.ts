class get_set{			//Compile using tsc --target es5 get_set.ts
	private x: number;

check(){
	console.log(this.x);
}

get a(){			
	return this.x;
}

set a(value){			
 this.x = value;
}
}

let ob = new get_set();
//let x = ob.a;
ob.a = 10;
ob.check();
ob.a(10);
ob.check();
