class cons{
	x: number;
	y: number;

constructor(x: number, y: number){	//inside class, can have access modifiers
	this.x = x;
	this.y = y;
}

draw(){
	console.log(this.x + this.y);
}
}

let ob= new cons(5,10);			//outside class
ob.draw();
