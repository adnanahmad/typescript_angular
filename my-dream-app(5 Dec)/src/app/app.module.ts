import { CoursesComponent } from './courses.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';                 //NgModule is a decorator

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CourseComponent } from './course/course.component';
import { CoursesService} from './courses.service';

@NgModule({                                               //decorator function
  declarations: [
    AppComponent,
    CourseComponent,
    CoursesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [                                            // for dependency injection
    CoursesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }