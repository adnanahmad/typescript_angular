//1. Template                                                  // A template is an HTML snippet that tells Angular how to render the component in angular application

/*import {Component} from '@angular/core';                        //{Component is a decorator}

@Component({                                                    // decorator function
    selector: 'mycourses',                                      //user defined selector    // arguments of the function
    //template: '<h2>{{"Title:" + title}}</h2>',                // arguments of the function
    template: '<h2>{{ getTitle() }}</h2>'                       // {{used for rendering something in our template dynamically}}
})

export class CoursesComponent{
    title = "List of courses";

    getTitle(){
        return this.title;
    }
}*/




//2. Directives                                                  // directives are used to manipulate(add,remove,change class of) the DOM element

/*import {Component} from '@angular/core';                        // {Component is a decorator}

@Component({                                                    // decorator function
    selector: 'mycourses',                                      // user defined selector    // arguments of the function
    template: `
                <h2>{{title}}</h2>
                <ul>
                <li *ngFor= "let course of courses">            
                    {{course}}
                </li>                                       
                </ul>
                `                                               // ngFor is a built-in template directive that makes it easy to iterate over something like an array or an object and create a template for each item
                                                                
                                                                // back tick helps break template into multiple line and make it more readable 
                                                                // {{used for rendering something in our template dynamically}}
})

export class CoursesComponent{
    title = "List of courses";

    courses = ["course1", "course2", "course3"];                // in real world instead strings we have objects
}*/




//3. Services and Dependency Injection                         // for reusing the code and unit testing

import {CoursesService} from './courses.service';

import {Component} from '@angular/core';                        // {Component is a decorator}

@Component({                                                    // decorator function
    selector: 'mycourses',                                      // user defined selector    // arguments of the function
    template: `
                <h2>{{title}}</h2>
                <ul>
                <li *ngFor= "let course of courses">            
                    {{course}}
                </li>                                       
                </ul>
                `                                               // ngFor is a built-in template directive that makes it easy to iterate over something like an array or an object and create a template for each item
                                                                // directives are used to manipulate(add,remove,change class of) the DOM element
                                                                // back tick helps break template into multiple line and make it more readable 
                                                                // {{used for rendering something in our template dynamically}}
})

export class CoursesComponent{
    title = "List of courses";

    courses;  
                                                                // Parameter has a dependency of type CoursesService
    constructor(service: CoursesService){                       // since we initialize an object in a constructor
        this.courses = service.getCourses();
    }              
}