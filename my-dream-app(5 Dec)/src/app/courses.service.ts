import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

 //constructor() { }
  getCourses(){
        return ["course1", "course2", "course3"];
    }

}



// No @Component() - Since no decorator function required for services, since service is a plane typescript class

//@Injectable()

/*export class CoursesService{
    getCourses(){
        return ["course1", "course2", "course3"];
    }

}*/