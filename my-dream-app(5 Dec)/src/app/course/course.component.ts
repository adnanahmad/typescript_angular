import { Component, OnInit } from '@angular/core';        // OnInit is an interface

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {          // OnInit is an interface, therefore implements

  constructor() { }

  ngOnInit() {
  }

}
