var get_set = /** @class */ (function () {
    function get_set() {
    }
    get_set.prototype.check = function () {
        console.log(this.x);
    };
    Object.defineProperty(get_set.prototype, "a", {
        get: function () {
            return this.x;
        },
        set: function (value) {
            this.x = value;
        },
        enumerable: true,
        configurable: true
    });
    return get_set;
}());
var ob = new get_set();
//let x = ob.a;
ob.a = 10;
ob.check();
ob.a(10);
ob.check();
