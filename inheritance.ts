class Car{
engineName: string;
gears: number;
speed: number;
constructor(){
this.speed=10;
}
accelerate(): void {
this.speed++;
}
throttle():void {
this.speed--;
}
getSpeed():void {
console.log(this.speed);
}
}

class Mazda extends Car{
accelerate(): void {
this.speed += 5;
}
throttle():void {
this.speed -= 5;
}
}

var obj = new Mazda(); 
obj.accelerate(); 
console.log(obj.speed)

obj.throttle(); 
console.log(obj.speed)
