import {Component} from '@angular/core';                        //{Component is a decorator}

@Component({                                                    // decorator function
    selector: 'courses',                                        // arguments of the function
    //template: '<h2>{{"Title:" + title}}</h2>',                // arguments of the function
    template: '<h2>{{ getTitle() }}</h2>'
})

export class CoursesComponent{
    title = "List of courses";

    getTitle(){
        return this.title;
    }
}